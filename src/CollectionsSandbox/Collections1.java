package CollectionsSandbox;


import java.util.*;
import java.lang.*;

public class Collections1 {

    public static void main(String[] args) {


        List<String> list = new ArrayList<>();
        list.add("List Item 1");
        list.add("List Item 2");
        list.add("List Item 3");
        list.add("List Item 4");
        list.add("List Item 5");
        list.add("List Item 5"); // Note "List Item 5" IS added twice.

        for (Object str : list) {
            System.out.println((String)str);
        }


        Set<String> set = new TreeSet<>();
        set.add("Set Item 1");
        set.add("Set Item 2");
        set.add("Set Item 3");
        set.add("Set Item 4");
        set.add("Set Item 5");
        set.add("Set Item 5"); // Note, "Set Item 5" IS NOT added twice.


        for (Object str : set) {
            System.out.println((String)str);
        }

        Queue<String> queue = new PriorityQueue<>();
        queue.add("qItem1");
        queue.add("qItem7");
        queue.add("qItem3");
        queue.add("qItem2");
        queue.add("qItem5");
        queue.add("qItem9");
        //"The elements of the priority queue are ordered according to their natural ordering,
        // or by a Comparator provided at queue construction time, depending on which
        // constructor is used." --Oracle Documentation  --- https://docs.oracle.com/javase/7/docs/api/java/util/PriorityQueue.html

        Iterator<String> iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }



        Map<Integer, String> map = new HashMap<>();
        map.put(1,"Slot 1");
        map.put(2,"Slot 2");
        map.put(3,"Slot 3");
        map.put(4,"Slot 4");
        map.put(5,"Slot 5");
        map.put(5,"Slot 6"); //intentionally replacing the index value "5"
        map.put(6,"Slot 7");



        for (int i = 1; i < 6; i++) {
            String result = map.get(i);
            System.out.println(result);
        }


        List<Motorcycle> myList = new LinkedList<>();
        myList.add(new Motorcycle(2018, "Honda", "CB300F", 300));
        myList.add(new Motorcycle(1997, "BMW", "R850R" ,850));
        myList.add(new Motorcycle(2016, "Honda", "CB300F", 300));
        myList.add(new Motorcycle(2004, "Kawasaki", "Ninja", 250));

        for (Motorcycle cycle : myList) {
            System.out.println(cycle);
            System.out.println("-----------------");
        }
        System.out.println("-------COMPARATOR----------");
        System.out.println("Unsorted");
        for (Motorcycle value : myList) System.out.println(value);

        myList.sort(new Motorcycle.SortYear());

        System.out.println("\nSorted by Year");
        for (Motorcycle motorcycle : myList) {
            System.out.println(motorcycle);

            System.out.println("-----------------");
        }
        System.out.println("-----------------");
        System.out.println("-----------------");
        System.out.println("-----------------");


        myList.sort(new Motorcycle.SortMake());

        System.out.println("\nSorted by Make");
        for (Motorcycle motorcycle : myList) {
            System.out.println(motorcycle);
            System.out.println("-----------------");
        }
    }
}